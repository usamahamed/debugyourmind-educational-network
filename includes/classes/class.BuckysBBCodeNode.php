<?php

abstract class usamaBBCodeNode {

    /**
     * Nodes parent
     *
     * @var usamaBBCodeNodeContainer
     */
    protected $parent;

    /**
     * Nodes root parent
     *
     * @var usamaBBCodeNodeContainer
     */
    protected $root;

    /**
     * Sets the nodes parent
     *
     * @param usamaBBCodeNode|usamaBBCodeNodeContainer $parent
     */
    public function set_parent(usamaBBCodeNodeContainer $parent = null){
        $this->parent = $parent;

        if($parent instanceof usamaBBCodeNodeContainerDocument)
            $this->root = $parent;else
            $this->root = $parent->root();
    }

    /**
     * Gets the nodes parent. Returns null if there
     * is no parent
     *
     * @return usamaBBCodeNode
     */
    public function parent(){
        return $this->parent;
    }

    /**
     * @return string
     */
    public function get_html(){
        return null;
    }

    /**
     * Gets the nodes root node
     *
     * @return usamaBBCodeNode
     */
    public function root(){
        return $this->root;
    }

    /**
     * Finds a parent node of the passed type.
     * Returns null if none found.
     *
     * @param string $tag
     * @return usamaBBCodeNodeContainerTag
     */
    public function find_parent_by_tag($tag){
        $node = $this->parent();

        while($this->parent() != null && !$node instanceof usamaBBCodeNodeContainerDocument){
            if($node->tag() === $tag)
                return $node;

            $node = $node->parent();
        }

        return null;
    }
}