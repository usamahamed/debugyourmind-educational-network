<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//If the user is not logged in, redirect to the index page
if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php');
}

if(isset($_POST['action']) && $_POST['action'] == 'change_password'){
    $isValid = true;
    if(!$_POST['userID'] || !$_POST['userIDHash'] || !usama_check_id_encrypted($_POST['userID'], $_POST['userIDHash']) || $userID != $_POST['userID']){
        usama_redirect("/index.php");
    }
    if(!$_POST['currentPassword'] || !$_POST['newPassword'] || !$_POST['newPassword2']){
        usama_redirect("/change_password.php", MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        $isValid = false;
    }else if($_POST['newPassword'] != $_POST['newPassword2']){
        usama_redirect("/change_password.php", MSG_NOT_MATCH_PASSWORD, MSG_TYPE_ERROR);
        $isValid = false;
    }else if(!usama_check_password_strength($_POST['newPassword'])){
        usama_redirect("/change_password.php", MSG_NEW_PASSWORD_STRENGTH_ERROR, MSG_TYPE_ERROR);
        $isValid = false;
    }

    //Check Current Password
    $data = usamaUser::getUserData($userID);
    if(!$data)
        usama_redirect("/index.php");

    if(!usama_validate_password($_POST['currentPassword'], $data['password'])){
        usama_redirect("/change_password.php", MSG_CURRENT_PASSWORD_NOT_CORRECT, MSG_TYPE_ERROR);
        $isValid = false;
    }
    if($isValid){
        $pwd = usama_encrypt_password($_POST['newPassword']);
        usamaUser::updateUserFields($userID, ['password' => $pwd]);
        usama_redirect('/change_password.php', MSG_PASSWORD_UPDATED);
    }

}

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('info.css');

$TNB_GLOBALS['content'] = 'change_password';

$TNB_GLOBALS['title'] = "Change Password - " . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
