<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

//If the parameter is null, goto homepage 
if($userID)
    usama_redirect('/account.php');

$token = isset($_REQUEST['token']) ? $_REQUEST['token'] : '';
if(!$token){
    usama_redirect('/index.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}

if(!($userID = usamaUsersToken::checkTokenValidity($token, 'password'))){
    usama_redirect('/register.php?forgotpwd=1', MSG_USER_TOKEN_LINK_NOT_CORRECT, MSG_TYPE_ERROR);
}

if(isset($_POST['action']) && $_POST['action'] == 'reset-password'){
    if(!$_POST['password'] || !$_POST['password']){
        usama_add_message(MSG_EMPTY_PASSWORD, MSG_TYPE_ERROR);
    }else if($_POST['password'] != $_POST['password']){
        usama_add_message(MSG_NOT_MATCH_PASSWORD, MSG_TYPE_ERROR);
    }else{
        $pwd = usama_encrypt_password($_POST['password']);
        usamaUser::updateUserFields($userID, ['password' => $pwd]);
        usama_redirect('/index.php', MSG_PASSWORD_UPDATED);
    }

}

usama_enqueue_stylesheet('register.css');

usama_enqueue_javascript('register.js');

$TNB_GLOBALS['content'] = 'reset_password';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
