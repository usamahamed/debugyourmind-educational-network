<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
if(!usama_check_user_acl(USER_ACL_ADMINISTRATOR)){
    usama_redirect('/index.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

if(isset($_REQUEST['action'])){
    if($_REQUEST['action'] == 'unban'){

        usamaBanUser::unbanUsers($_REQUEST['bannedID']);
        usama_redirect('/banned_users.php', MSG_UNBAN_USERS);
    }else if($_REQUEST['action'] == 'delete'){
        usamaBanUser::deleteUsers($_REQUEST['bannedID']);
        usama_redirect('/banned_users.php', MSG_DELETE_USERS);
    }else if($_REQUEST['action'] == 'deletebyid'){

        if(!isset($_REQUEST['userID']) || !usamaUser::checkUserID($_REQUEST['userID'], false)){
            usama_redirect('/index.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        usamaUser::deleteUserAccount($_REQUEST['userID']);
        usama_redirect('/index.php', MSG_DELETE_USERS);
    }
    exit;
}

$page = isset($_GET['page']) ? $_GET['page'] : 1;
$totalCount = usamaBanUser::getBannedUsersCount();

//Init Pagination Class
$pagination = new Pagination($totalCount, usamaBanUser::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

$users = usamaBanUser::getBannedUsers($page, usamaBanUser::$COUNT_PER_PAGE);

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('moderator.css');

usama_enqueue_javascript('banned_users.js');

$TNB_GLOBALS['content'] = 'banned_users';
$TNB_GLOBALS['title'] = "Manage Banned Users - " . TNB_SITE_NAME;
require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
