<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!usama_check_user_acl(USER_ACL_REGISTERED)){
    usama_redirect('/forum', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

//======= Check if permission allowed ===========//
$permissionAllowed = false;
$forumTopicIns = new usamaForumTopic();
$userID = usama_is_logged_in();

$topicID = isset($_REQUEST['id']) ? get_secure_integer($_REQUEST['id']) : null;
if(isset($topicID)){
    $forumData = $forumTopicIns->getTopic($topicID);

    if(isset($forumData) && $forumData['creatorID'] == $userID){
        $permissionAllowed = true;
    }
}

if($permissionAllowed == false){
    usama_redirect("/forum/topic.php?id=" . $topicID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

if(isset($_POST['action'])){
    if($_POST['action'] == 'edit-topic'){

        $result = $forumTopicIns->editTopic($_POST);
        if($result === true){
            usama_redirect("/forum/topic.php?id=" . $topicID, MSG_TOPIC_POSTED_SUCCESSFULLY, MSG_TYPE_SUCCESS);
        }else{
            usama_redirect("/forum/edit_topic.php?id=" . $topicID, $result, MSG_TYPE_ERROR);
        }
    }
}

$categoryID = $forumData['categoryID'];
$category = usamaForumCategory::getCategory($categoryID);

$categories = usamaForumCategory::getAllCategories();

usama_enqueue_stylesheet('sceditor/themes/default.css');
usama_enqueue_stylesheet('forum.css');
usama_enqueue_stylesheet('publisher.css');
usama_enqueue_stylesheet('uploadify.css');

usama_enqueue_javascript('sceditor/jquery.sceditor.bbcode.js');
usama_enqueue_javascript('uploadify/jquery.uploadify.js');

$view['action_type'] = 'edit';
$view['forum_data'] = $forumData;

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/create_topic';
$TNB_GLOBALS['title'] = 'Edit Topic - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");

