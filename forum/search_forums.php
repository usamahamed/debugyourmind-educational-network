<?php

require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

$categoryID = null;
if(isset($_REQUEST['id'])){
    $categoryID = usama_escape_query_integer($_REQUEST['id']);
    $category = usamaForumCategory::getCategory($_REQUEST['id']);
}

$keyword = isset($_REQUEST['s']) ? usama_escape_query_string($_GET['s']) : '';

$orderBy = isset($_GET['orderby']) ? usama_escape_query_string($_GET['orderby']) : 'popular';

switch($orderBy){
    case 'recent':
        $orderByStr = ' c.createdDate DESC ';
        break;
    case 'popular':
    default:
        $orderByStr = ' c.followers DESC ';
        break;
}

$page = isset($_GET['page']) ? usama_escape_query_integer($_GET['page']) : 1;

$results = usamaForumCategory::searchCategories($keyword, $page, $orderByStr, usamaForumCategory::$COUNT_PER_PAGE);

$pagination = new Pagination($results['total'], usamaForumCategory::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

usama_enqueue_stylesheet('sceditor/themes/default.css');
usama_enqueue_stylesheet('forum.css');
usama_enqueue_stylesheet('publisher.css');
usama_enqueue_stylesheet('uploadify.css');

usama_enqueue_javascript('sceditor/jquery.sceditor.bbcode.js');
usama_enqueue_javascript('uploadify/jquery.uploadify.js');

$view['action_type'] = 'create';

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/search_forums';
$TNB_GLOBALS['title'] = 'Search Forums - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");