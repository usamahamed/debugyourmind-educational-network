<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!usama_check_user_acl(USER_ACL_ADMINISTRATOR) && !usamaModerator::isModerator($TNB_GLOBALS['user']['userID'])){
    usama_redirect('/forum', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

//Process Post Actions
if(isset($_POST['action'])){
    $action = $_POST['action'];
    //Approve Topics
    if($action == 'approve-topic'){
        //Getting Ids
        $topicIds = isset($_POST['tid']) ? $_POST['tid'] : null;
        if(!$topicIds)
            usama_redirect('/forum/pending_topcis.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);

        $result = usamaForumTopic::approvePendingTopics($topicIds);
        if($result === true)
            usama_redirect('/forum/pending_topics.php', MSG_TOPIC_APPROVED_SUCCESSFULLY);else
            usama_redirect('/forum/pending_topics.php', $result, MSG_TYPE_ERROR);
    }else if($action == 'delete-topic'){ // Delete Pending Topics
        //Getting Ids
        $topicIds = isset($_POST['tid']) ? $_POST['tid'] : null;
        if(!$topicIds)
            usama_redirect('/forum/pending_topcis.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);

        $result = usamaForumTopic::deletePendingTopics($topicIds);
        if($result === true)
            usama_redirect('/forum/pending_topics.php', MSG_TOPIC_REMOVED_SUCCESSFULLY);else
            usama_redirect('/forum/pending_topics.php', $result, MSG_TYPE_ERROR);
    }

}

//Getting Pending Topics
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$total = usamaForumTopic::getTotalNumOfTopics('pending');

$pagination = new Pagination($total, usamaForumTopic::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

$topics = usamaForumTopic::getTopics($page, 'pending', null, null, usamaForumTopic::$COUNT_PER_PAGE);

usama_enqueue_javascript('jquery-migrate-1.2.0.js');

usama_enqueue_stylesheet('forum.css');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/pending_topics';
$TNB_GLOBALS['title'] = 'Pending Topics - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");

