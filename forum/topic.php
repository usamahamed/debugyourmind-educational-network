<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(isset($_POST['action'])){
    if($_POST['action'] == 'thumb-up' || $_POST['action'] == 'thumb-down'){
        if(!usama_check_user_acl(USER_ACL_REGISTERED)){
            $data = ['status' => 'error', 'message' => MSG_PLEASE_LOGIN_TO_CAST_VOTE];
        }else{
            if(!$_POST['objectID'] || !$_POST['objectIDHash'] || !$_POST['objectType'] || !usama_check_id_encrypted($_POST['objectID'], $_POST['objectIDHash'])){
                $data = ['status' => 'error', 'message' => MSG_INVALID_REQUEST];
            }else{
                if($_POST['objectType'] == 'topic')
                    $result = usamaForumTopic::voteTopic($TNB_GLOBALS['user']['userID'], $_POST['objectID'], $_POST['action'] == 'thumb-up' ? 1 : -1);else
                    $result = usamaForumReply::voteReply($TNB_GLOBALS['user']['userID'], $_POST['objectID'], $_POST['action'] == 'thumb-up' ? 1 : -1);
                if(is_int($result)){
                    $data = ['status' => 'success', 'message' => MSG_THANKS_YOUR_VOTE, 'votes' => ($result > 0 ? "+" : "") . $result];
                }else{
                    $data = ['status' => 'error', 'message' => $result];
                }
            }
        }
        render_result_xml($data);
        exit;
    }
}else if(isset($_GET['action']) && $_GET['action'] == 'delete'){
    //Delete this topic

    $userID = usama_is_logged_in();

    $topicID = isset($_GET['id']) ? get_secure_integer($_GET['id']) : null;

    if(isset($topicID)){
        $forumTopicIns = new usamaForumTopic();
        $forumData = $forumTopicIns->getTopic($topicID);

        if(isset($forumData) && $forumData['creatorID'] == $userID){
            //then you can delete this one.
            $forumTopicIns->deleteTopic($topicID);

            usama_redirect('/forum', MSG_TOPIC_REMOVED_SUCCESSFULLY, MSG_TYPE_SUCCESS);

        }else{
            //You don't have permission
            usama_redirect('/forum/topic.php?id=' . $topicID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);

        }

    }

}else if(isset($_GET['action']) && $_GET['action'] == 'move-topic'){

    //Delete this topic
    if(!usama_check_user_acl(USER_ACL_MODERATOR)){
        usama_redirect('/forum', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
    }
    $userID = usama_is_logged_in();

    $topicID = isset($_GET['id']) ? usama_escape_query_integer($_GET['id']) : null;

    $catID = isset($_GET['category']) ? usama_escape_query_integer($_GET['category']) : null;

    if(!$topicID){
        usama_redirect('/forum', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }

    if(!$catID || !($category = usamaForumCategory::getCategory($catID))){
        usama_redirect('/forum/topic.php?id=' . $topicID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }

    $forumTopicIns = new usamaForumTopic();
    $forumTopicIns->moveTopic($topicID, $catID);

    usama_redirect('/forum/topic.php?id=' . $topicID, MSG_TOPIC_MOVED_SUCCESSFULLY);
}

$topicID = isset($_GET['id']) ? usama_escape_query_integer($_GET['id']) : 0;

$topic = usamaForumTopic::getTopic($topicID);

if(!$topic)
    usama_redirect('/forum');

$category = usamaForumCategory::getCategory($topic['categoryID']);

//If the topic is not published(pending or suspended), only forum moderator and administrator can see this
if($topic['status'] != 'publish' && !usama_is_moderator() && $TNB_GLOBALS['user']['userID'] != $topic['creatorID'])
    usama_redirect('/forum');

$orderBy = isset($_GET['orderby']) ? usama_escape_query_string($_GET['orderby']) : 'oldest';

//Getting Replies
$page = isset($_GET['page']) ? usama_escape_query_integer($_GET['page']) : 1;
$total = usamaForumReply::getTotalNumOfReplies($topic['topicID'], 'publish');

$pagination = new Pagination($total, usamaForumReply::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

$replies = usamaForumReply::getReplies($topic['topicID'], 'publish', $page, $orderBy);

$hierarchical = usamaForumCategory::getCategoryHierarchical($topic['categoryID']);

//Mark Forum Notifications to read
if(usama_check_user_acl(USER_ACL_REGISTERED))
    usamaForumNotification::makeNotificationsToRead($TNB_GLOBALS['user']['userID'], null, $topic['topicID']);

if(usama_check_user_acl(USER_ACL_MODERATOR)){
    $reportID = usamaReport::isReported($topicID, 'topic');
    $categories = usamaForumCategory::getAllCategories();
}

usama_enqueue_javascript('sceditor/jquery.sceditor.bbcode.js');
usama_enqueue_javascript('uploadify/jquery.uploadify.js');

usama_enqueue_javascript('highlight.pack.js');
usama_enqueue_javascript('forum.js');

usama_enqueue_stylesheet('sceditor/themes/default.css');
usama_enqueue_stylesheet('obsidian.css');
usama_enqueue_stylesheet('forum.css');
usama_enqueue_stylesheet('publisher.css');
usama_enqueue_stylesheet('uploadify.css');

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/topic';
$TNB_GLOBALS['title'] = $topic['topicTitle'] . ' - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");

