<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

$categoryID = isset($_GET['id']) ? $_GET['id'] : 0;

if(isset($_REQUEST['action'])){
    if($_REQUEST['action'] == 'follow' || $_REQUEST['action'] == 'unfollow'){
        if(!($userID = usama_is_logged_in()) && usama_check_form_token('request')){
            usama_redirect(isset($_REQUEST['return']) ? base64_decode($_REQUEST['return']) : '/forum', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        $category = usamaForumCategory::getCategory($categoryID);

        if(!$category || ($_REQUEST['action'] == 'follow' && usamaForumFollower::isFollow($category['categoryID'], $userID)) || ($_REQUEST['action'] == 'unfollow' && !usamaForumFollower::isFollow($category['categoryID'], $userID)) || $category['creatorID'] == $userID){
            usama_redirect(isset($_REQUEST['return']) ? base64_decode($_REQUEST['return']) : '/forum', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        if($_REQUEST['action'] == 'follow'){
            usamaForumFollower::followForum($userID, $categoryID);
            usama_add_message(MSG_FOLLOW_FORUM_SUCCESS);
        }else{
            usamaForumFollower::unfollowForum($userID, $categoryID);
            usama_add_message(MSG_UNFOLLOW_FORUM_SUCCESS);
        }
        usama_redirect(isset($_REQUEST['return']) ? base64_decode($_REQUEST['return']) : '/forum');
    }
}

$category = usamaForumCategory::getCategory($categoryID);
if(!$category){
    usama_redirect('/forum');
}

//Getting Topics by category id
$page = isset($_GET['page']) ? $_GET['page'] : 1;

$orderby = isset($_GET['orderby']) ? $_GET['orderby'] : 'recent';
switch($orderby){
    case 'recent':
        $orderbyString = 'lastReplyDate DESC';
        break;
    case 'rating':
        $orderbyString = 't.votes DESC';
        break;
    case 'replies':
        $orderbyString = 't.replies DESC';
        break;
}

$total = usamaForumTopic::getTotalNumOfTopics('publish', $category['categoryID']);

$pagination = new Pagination($total, usamaForumTopic::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

$topics = usamaForumTopic::getTopics($page, 'publish', $category['categoryID'], $orderbyString, usamaForumTopic::$COUNT_PER_PAGE);

$hierarchical = usamaForumCategory::getCategoryHierarchical($category['categoryID']);

//Mark Forum Notifications to read
if(usama_check_user_acl(USER_ACL_REGISTERED))
    usamaForumNotification::makeNotificationsToRead($TNB_GLOBALS['user']['userID'], $category['categoryID']);

usama_enqueue_javascript('jquery-migrate-1.2.0.js');

usama_enqueue_stylesheet('forum.css');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/category';
$TNB_GLOBALS['title'] = $category['categoryName'] . ' - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");


