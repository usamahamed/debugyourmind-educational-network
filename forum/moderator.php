<?php

require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/forum', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}

//Check Category ID
$categoryID = isset($_REQUEST['id']) ? usama_escape_query_integer($_REQUEST['id']) : 0;
if(!$categoryID || !($category = usamaForumCategory::getCategory($categoryID))){
    usama_redirect('/forum', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}

if(isset($_REQUEST['action'])){
    if($_REQUEST['action'] == 'apply-moderate'){
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']) || usama_is_forum_moderator($category['categoryID'])){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        //Check if already applied
        if(usamaForumModerator::isAppliedToModerate($category['categoryID'])){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_ALREADY_APPLIED_TO_MODERATE, MSG_TYPE_ERROR);
        }

        if(usamaForumModerator::applyToModerate($category['categoryID'], $userID)){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_APPLY_TO_MODERATE_SUCCESS);
        }else{
            usama_redirect('/forum/category.php?id=' . $categoryID, $db->getLastError(), MSG_TYPE_ERROR);
        }

    }else if($_REQUEST['action'] == 'Accept'){
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }
        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']))){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        $applicants = isset($_POST['applicant']) ? $_POST['applicant'] : null;
        if(!$applicants){
            usama_add_message(MSG_NO_APPLICANTS_SELECTED, MSG_TYPE_ERROR);
        }else{
            usamaForumModerator::approveApplicants($categoryID, $applicants);
            usama_redirect("/forum/moderator.php?id=" . $categoryID, MSG_APPLICANTS_APPROVED);
        }
    }else if($_REQUEST['action'] == 'Decline'){
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }
        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']))){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        $applicants = isset($_POST['applicant']) ? $_POST['applicant'] : null;
        if(!$applicants){
            usama_add_message(MSG_NO_APPLICANTS_SELECTED, MSG_TYPE_ERROR);
        }else{
            usamaForumModerator::declineApplicants($categoryID, $applicants);
            usama_redirect("/forum/moderator.php?id=" . $categoryID, MSG_APPLICANTS_DECLINED);
        }
    }else if($_REQUEST['action'] == 'delete-moderator'){
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }
        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']))){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        $moderator = usama_escape_query_integer($_REQUEST['moderator']);
        if($category['creatorID'] == $moderator){
            usama_redirect('/forum/moderator.php?id=' . $categoryID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        usamaForumModerator::deleteModerator($categoryID, $moderator);
        usama_redirect("/forum/moderator.php?id=" . $categoryID, MSG_MODERATOR_REMOVED);
    }else if($_REQUEST['action'] == 'Delete'){
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']) || usama_is_forum_moderator($category['categoryID']))){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }
        usamaReport::deleteObjects($_REQUEST['reportID']);
        usama_redirect("/forum/moderator.php?id=" . $categoryID, MSG_REPORTED_OBJECT_REMOVED);
    }else if($_REQUEST['action'] == 'Approve'){
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']) || usama_is_forum_moderator($category['categoryID']))){
            usama_redirect('/forum/category.php?id=' . $categoryID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }
        usamaReport::approveObjects($_REQUEST['reportID']);
        usama_redirect("/forum/moderator.php?id=" . $categoryID, MSG_REPORTED_OBJECT_APPROVED);
    }else if($_REQUEST['action'] == 'block-user'){
        $return = isset($_REQUEST['return']) ? base64_decode($_REQUEST['return']) : ('/forum/category.php?id=' . $categoryID);
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect($return, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        //Admin, Site Moderator, Category Admin and Category Moderator can't be blocked
        if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']) || usama_is_forum_moderator($category['categoryID']))){
            usama_redirect($return, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        $blockedUserID = usama_escape_query_integer($_REQUEST['userID']);

        if($blockedUserID == $userID){
            usama_redirect($return, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        usamaForumModerator::blockUser($blockedUserID, $category['categoryID']);

        usama_redirect($return, MSG_BLOCK_USER_SUCCESS);
    }else if($_REQUEST['action'] == 'unblock-users'){
        $return = isset($_REQUEST['return']) ? base64_decode($_REQUEST['return']) : ('/forum/moderator.php?id=' . $categoryID);
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect($return, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']) || usama_is_forum_moderator($category['categoryID']))){
            usama_redirect($return, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        $blockedUsers = isset($_REQUEST['blocked_user']) ? $_REQUEST['blocked_user'] : null;

        if(!$blockedUsers){
            usama_redirect($return, MSG_NO_USER_SELECTED, MSG_TYPE_ERROR);
        }

        foreach($blockedUsers as $bUser){
            usamaForumModerator::unBlockUser($bUser, $category['categoryID']);
        }

        usama_redirect($return, MSG_UNBLOCK_USER_SUCCESS);
    }else if($_REQUEST['action'] == 'delete-forum'){
        $return = isset($_REQUEST['return']) ? base64_decode($_REQUEST['return']) : ('/forum/moderator.php?id=' . $categoryID);
        //Check forum token
        if(!usama_check_form_token('request')){
            usama_redirect($return, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        //Admin, Site Moderator, Category Admin and Category Moderator can't apply
        if(!(usama_is_admin() || usama_is_forum_admin($category['categoryID']))){
            usama_redirect($return, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        //Check Password
        $userData = usamaUser::getUserData($userID);
        if(!usama_validate_password($_REQUEST['pwd'], $userData['password'])){
            usama_redirect($return, MSG_CURRENT_PASSWORD_NOT_CORRECT, MSG_TYPE_ERROR);
        }

        usamaForumCategory::deleteCategory($category['categoryID']);
        usama_redirect("/forum", MSG_REMOVE_FORUM_SUCCESS);
    }
}

//Admin, Site Moderator, Category Admin and Category Moderator can't apply
if(!(usama_is_admin() || usama_is_moderator() || usama_is_forum_admin($category['categoryID']) || usama_is_forum_moderator($category['categoryID']))){
    usama_redirect('/forum/category.php?id=' . $categoryID, MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

//Getting Reported Posts
$reported_posts = usamaForumModerator::getReportedArticles($categoryID);

//Getting Applicants
$applicants = usamaForumModerator::getApplicants($categoryID);

$blockedUsers = usamaForumModerator::getBlockedUsers($categoryID);

usama_enqueue_stylesheet('sceditor/themes/default.css');
usama_enqueue_stylesheet('forum.css');

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/moderator';

$TNB_GLOBALS['title'] = $category['categoryName'] . ' Moderator Panel - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");








