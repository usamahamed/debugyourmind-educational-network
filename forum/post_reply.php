<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/forum', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

$topicID = isset($_GET['id']) ? $_GET['id'] : 0;

$topic = usamaForumTopic::getTopic($topicID);
$forumReplyIns = new usamaForumReply();
$view = [];
$view['action_type'] = 'create';

if(!$topic)
    usama_redirect('/forum');

if(isset($_POST['action'])){
    if($_POST['action'] == 'post-reply'){
        $result = usamaForumReply::createReply($_POST);
        if($result == 'pending' || $result == 'publish'){
            usama_redirect("/forum/topic.php?id=" . $topicID, MSG_REPLY_POSTED_SUCCESSFULLY . ($result == 'pending' ? ' ' . MSG_POST_IS_UNDER_PREVIEW : ''), MSG_TYPE_SUCCESS);
        }else{
            usama_redirect("/forum/topic.php.php?id=" . $topicID, $result, MSG_TYPE_ERROR);
        }
    }else if($_POST['action'] == 'edit-post-reply'){

        $userID = usama_is_logged_in();
        $replyID = isset($_REQUEST['replyID']) ? get_secure_integer($_REQUEST['replyID']) : null;
        $replyData = $forumReplyIns->getReplyByID($replyID);

        if($replyData && $replyData['creatorID'] == $userID && $replyData['topicID'] == $topicID){

            $result = $forumReplyIns->editReply($_POST);

            if($result == 'pending' || $result == 'publish'){

                usama_redirect("/forum/topic.php?id=" . $topicID, MSG_REPLY_POSTED_SUCCESSFULLY, MSG_TYPE_SUCCESS);

            }else{

                $replyID = isset($_REQUEST['replyID']) ? get_secure_integer($_REQUEST['replyID']) : null;
                usama_redirect("/forum/post_reply.php?id=" . $topicID . '&action=edit&replyID=' . $replyID, $result, MSG_TYPE_ERROR);

            }
        }else{
            //no permission
            //permission error
            usama_redirect('/forum', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

    }
}else if(isset($_GET['action']) && $_GET['action'] == 'delete'){
    //Delete post_reply

    $userID = usama_is_logged_in();
    $replyID = isset($_GET['replyID']) ? get_secure_integer($_GET['replyID']) : null;
    $replyData = $forumReplyIns->getReplyByID($replyID);

    if($replyData && $replyData['creatorID'] == $userID && $replyData['topicID'] == $topicID){
        //then you can delete this one
        $forumReplyIns->deleteReply($replyID);
        usama_redirect("/forum/topic.php?id=" . $topicID, MSG_REPLY_REMOVED_SUCCESSFULLY, MSG_TYPE_SUCCESS);
    }else{
        echo MSG_PERMISSION_DENIED;
    }

    exit;

}else if(isset($_GET['action']) && $_GET['action'] == 'edit'){

    //edit post_reply

    $forumReplyIns = new usamaForumReply();
    $userID = usama_is_logged_in();
    $replyID = isset($_GET['replyID']) ? get_secure_integer($_GET['replyID']) : null;
    $replyData = $forumReplyIns->getReplyByID($replyID);

    if($replyData && $replyData['creatorID'] == $userID && $replyData['topicID'] == $topicID){
        //then you can edit this one
        $view['replyData'] = $replyData;
        $view['action_type'] = 'edit';
        $view['replyID'] = $replyID;
    }else{
        //permission error
        usama_redirect('/forum', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
    }
}

if(!isset($replyData) || !$replyData){
    usama_redirect("/forum", MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}

$topicData = usamaForumTopic::getTopic($replyData['replyID']);
$category = usamaForumCategory::getCategory($topicData['categoryID']);

$categories = usamaForumCategory::getAllCategories();

usama_enqueue_stylesheet('sceditor/themes/default.css');
usama_enqueue_stylesheet('forum.css');
usama_enqueue_stylesheet('publisher.css');
usama_enqueue_stylesheet('uploadify.css');

usama_enqueue_javascript('sceditor/jquery.sceditor.bbcode.js');
usama_enqueue_javascript('uploadify/jquery.uploadify.js');

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/post_reply';
$TNB_GLOBALS['title'] = 'Post Reply - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");

