<?php
require_once(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect("/forum/home.php", MSG_INVALID_REQUEST);
}

//Getting Topics by category id
$page = isset($_GET['page']) ? $_GET['page'] : 1;

$total = usamaForumTopic::getTotalNumOfUserTopics($userID);

$pagination = new Pagination($total, usamaForumTopic::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

$topics = usamaForumTopic::getUserTopics($userID, $page, 'lastReplyDate DESC, t.createdDate DESC', usamaForumTopic::$COUNT_PER_PAGE);

usama_enqueue_javascript('jquery-migrate-1.2.0.js');

usama_enqueue_stylesheet('forum.css');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['headerType'] = 'forum';
$TNB_GLOBALS['content'] = 'forum/home';
$TNB_GLOBALS['title'] = 'My Forum Feed - thenewboston Forum';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");


