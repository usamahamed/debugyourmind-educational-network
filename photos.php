<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

//Getting User ID from Parameter
$profileID = get_secure_integer($_GET['user']);

$albumID = isset($_GET['albumID']) ? usama_escape_query_integer($_GET['albumID']) : null;
$postID = isset($_GET['post']) ? usama_escape_query_integer($_GET['post']) : null;

//When displaying page's photo
$showPagePhotoFlag = false;
$paramPageID = usamaPost::INDEPENDENT_POST_PAGE_ID;

$pageData = null;
if(isset($_GET['pid'])){
    $paramPageID = $_GET['pid'];
    $pageIns = new usamaPage();
    $pageData = $pageIns->getPageByID($paramPageID);
    if($pageData){
        $profileID = $pageData['userID'];
        $showPagePhotoFlag = true;
    }
}

//If the parameter is null, goto homepage 
if(!$profileID)
    usama_redirect('/index.php');

//Getting UserData from Id
$userData = usamaUser::getUserData($profileID);

//Goto Homepage if the userID is not correct
if(!usama_not_null($userData) || !usamaUser::checkUserID($profileID, true)){
    usama_redirect('/index.php');
}

if(!$showPagePhotoFlag){
    //if logged user can see all resources of the current user
    $canViewPrivate = $userID == $profileID || usamaFriend::isFriend($userID, $profileID) || usamaFriend::isSentFriendRequest($profileID, $userID);

    $photos = usamaPost::getPhotosByUserID($profileID, $userID, $paramPageID, $canViewPrivate, $postID, $albumID, usamaPost::$images_per_page);

    $albums = usamaAlbum::getAlbumsByUserId($profileID);

    //Display
    $TNB_GLOBALS['title'] = trim($userData['firstName'] . ' ' . $userData['lastName']) . "'s Photos - " . TNB_SITE_NAME;
    $view['photo_type'] = 'profile';

    usama_enqueue_stylesheet('profile.css');
    usama_enqueue_stylesheet('posting.css');
    usama_enqueue_stylesheet('publisher.css');

    usama_enqueue_javascript('posts.js');
}else{
    //Show page photos if logged user can see all resources of the current user
    $photos = usamaPost::getPhotosByUserID($profileID, null, $paramPageID, false, $postID, $albumID, usamaPost::$images_per_page);

    //Display
    $TNB_GLOBALS['title'] = trim($pageData['title']) . "'s Photos - " . TNB_SITE_NAME;
    $view['photo_type'] = 'page';
    $view['pageData'] = $pageData;

    usama_enqueue_stylesheet('account.css');
    usama_enqueue_stylesheet('stream.css');
    usama_enqueue_stylesheet('posting.css');
    usama_enqueue_stylesheet('uploadify.css');
    usama_enqueue_stylesheet('jquery.Jcrop.css');
    usama_enqueue_stylesheet('page.css');
    usama_enqueue_stylesheet('publisher.css');

    usama_enqueue_javascript('uploadify/jquery.uploadify.js');
    usama_enqueue_javascript('jquery.Jcrop.js');
    usama_enqueue_javascript('jquery.color.js');

    usama_enqueue_javascript('posts.js');
    usama_enqueue_javascript('add_post.js');
    usama_enqueue_javascript('page.js');
}

$TNB_GLOBALS['content'] = 'photos';
require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");

