<?php

require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

//Getting User ID from Parameter
$profileID = isset($_GET['user']) ? intval($_GET['user']) : 0;

//If the parameter is null, goto homepage 
if(!$profileID)
    usama_redirect('/index.php');

//Getting UserData from Id
$userData = usamaUser::getUserData($profileID);

//Goto Homepage if the userID is not correct
if(!usama_not_null($userData) || !usamaUser::checkUserID($profileID, true)){
    usama_redirect('/index.php');
}

//Get this user followed page info
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 1;
$pageFollowerIns = new usamaPageFollower();
$totalCount = $pageFollowerIns->getPagesCountByFollowerID($profileID);

$pagination = new Pagination($totalCount, usamaPageFollower::COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

//Get Pages
$view['pages'] = $pageFollowerIns->getPagesByFollowerID($profileID, $page, usamaPageFollower::COUNT_PER_PAGE);
$view['profileID'] = $profileID;

//if logged user can see all resources of the current user
$canViewPrivate = $userID == $profileID || usamaFriend::isFriend($userID, $profileID) || usamaFriend::isSentFriendRequest($profileID, $userID);

usama_enqueue_stylesheet('profile.css');
usama_enqueue_stylesheet('friends.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['content'] = 'follows';

//Page title
$TNB_GLOBALS['title'] = trim($userData['firstName'] . ' ' . $userData['lastName']) . "'s Pages Followed - thenewboston";

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
