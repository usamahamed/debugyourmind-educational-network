<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

//Getting UserData from Id
$userData = usamaUser::getUserData($userID);

if(isset($_REQUEST['action'])){
    $action = $_REQUEST['action'];
    if($action == 'set-profile-photo'){
        usamaUser::updateUserProfilePhoto($userID, $_REQUEST['photoID']);
        usama_redirect('/photo_manage.php');
    }else if($action == 'delete-photo'){

        if(!usamaPost::deletePost($userID, $_REQUEST['photoID']))
            usama_redirect('/photo_manage.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);else
            usama_redirect('/photo_manage.php', MSG_PHOTO_REMOVED, MSG_TYPE_SUCCESS);
    }else if($action == 'remove-profile-photo'){
        usamaUser::updateUserFields($userID, ['thumbnail' => '']);
        usama_redirect('/photo_manage.php');
    }
}

//Getting Album ID
$albumID = isset($_REQUEST['albumID']) ? $_REQUEST['albumID'] : null;

//Getting Current Page
$page = isset($_GET['page']) ? $_GET['page'] : 1;

$totalCount = usamaPost::getNumberOfPhotosByUserID($userID, usamaPost::INDEPENDENT_POST_PAGE_ID, $albumID);

$pagination = new Pagination($totalCount, usamaPost::$IMAGES_PER_PAGE_FOR_MANAGE_PHOTOS_PAGE, $page);
$page = $pagination->getCurrentPage();

$photos = usamaPost::getPhotosByUserID($userID, $userID, usamaPost::INDEPENDENT_POST_PAGE_ID, true, null, $albumID, usamaPost::$IMAGES_PER_PAGE_FOR_MANAGE_PHOTOS_PAGE);

$albums = usamaAlbum::getAlbumsByUserId($userID);

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('info.css');

$TNB_GLOBALS['content'] = 'photo_manage';

$TNB_GLOBALS['title'] = "Manage Photos - " . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
