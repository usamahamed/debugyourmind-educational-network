<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

$albumID = isset($_REQUEST['albumID']) ? $_REQUEST['albumID'] : '';

if(!$albumID || !usamaAlbum::checkAlbumOwner($albumID, $userID)){
    usama_redirect("/photo_albums.php", MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}

//Getting UserData from Id
$userData = usamaUser::getUserData($profileID);

//Getting Album
$album = usamaAlbum::getAlbum($albumID);

//Getting Photos
$myphotos = usamaPost::getPhotosByUserID($userID, $userID, usamaPost::INDEPENDENT_POST_PAGE_ID, true);

$albumPhotos = usamaAlbum::getPhotos($albumID);

//Getting Album Photos

if(isset($_POST['action'])){
    //Create New Album
    if($_POST['action'] == 'save-album'){
        //If the album title is empty, throw error
        //If the album title is empty, throw error
        if(trim($_POST['album_name']) == ''){
            usama_redirect('/photo_album_edit.php?albumID=' . $_POST['albumID'], MSG_ALBUM_TITLE_EMPTY, MSG_TYPE_ERROR);
        }
        usamaAlbum::updateAlbum($_POST['albumID'], trim($_POST['album_name']), $_POST['visibility'], $_POST['photos']);
        usama_redirect("/photo_album_edit.php?albumID=" . $_POST['albumID'], MSG_ALBUM_UPDATED);
    }else if($_POST['action'] == 'remove-from-album' || $_POST['action'] == 'add-to-album'){
        $photoID = $_POST['photoID'];
        $photo = usamaPost::getPostById($photoID);

        //Check Photo Owner
        if($photo['poster'] != $userID){
            echo MSG_INVALID_REQUEST;
            exit;
        }
        if($_POST['action'] == 'remove-from-album')
            usamaAlbum::removePhotoFromAlbum($albumID, $photoID); //Remove
        else
            usamaAlbum::addPhotoToAlbum($albumID, $photoID); //Add
        echo 'success';
        exit;
    }
}

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('posting.css');

usama_enqueue_javascript('album.js');

$TNB_GLOBALS['content'] = 'photo_album_edit';

$TNB_GLOBALS['title'] = "Edit Photo Album - " . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
