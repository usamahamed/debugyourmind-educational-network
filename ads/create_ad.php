<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

//Getting Current User ID
if(!usama_check_user_acl(USER_ACL_REGISTERED)){
    usama_redirect('/index.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

$userID = usama_is_logged_in();

$adClass = new usamaAds();

if(isset($_POST['action']) && $_POST['action'] == 'create-ad'){
    if(!usama_check_form_token()){
        usama_redirect('/ads/create_ad.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }else{
        if($adClass->saveAd($userID, $_POST)){
            usama_redirect('/ads/advertiser.php?status=pending', $adClass->last_message);
        }else{
            usama_redirect('/ads/create_ad.php?type=' . $_POST['type'], $adClass->last_message, MSG_TYPE_ERROR);
        }
    }
}

$adSizes = $adClass->getAdSizes();

$adType = isset($_GET['type']) && $_GET['type'] == 'Image' ? 'Image' : 'Text';

usama_enqueue_stylesheet('publisher.css');
usama_enqueue_stylesheet('uploadify.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.number.js');
usama_enqueue_javascript('create_ad.js');

$TNB_GLOBALS['headerType'] = "ads";
$TNB_GLOBALS['content'] = "ads/create_ad";

$TNB_GLOBALS['title'] = "Create New Ad - thenewboston Ads";

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
