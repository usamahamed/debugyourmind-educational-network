<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

//Getting Current User ID
if(!usama_check_user_acl(USER_ACL_REGISTERED)){
    usama_redirect('/index.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

$userID = usama_is_logged_in();

$classPublisherAd = new usamaPublisherAds();

usama_enqueue_stylesheet('publisher.css');

$adID = usama_escape_query_integer($_GET['id']);

$adDetail = $classPublisherAd->getAdById($adID);

if(!$adDetail || ($adDetail['publisherID'] != $userID && usama_check_user_acl(USER_ACL_MODERATOR))){
    usama_redirect('/ads/publisher.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}

$classAds = new usamaAds();

$sizeDetail = $classAds->getAdSizeById($adDetail['size']);

$TNB_GLOBALS['headerType'] = "ads";
$TNB_GLOBALS['content'] = "ads/publisher_ad_view";

$TNB_GLOBALS['title'] = "View Ad Details - thenewboston Ads";

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
