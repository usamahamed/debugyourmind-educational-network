<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

//Getting Current User ID
if(!usama_check_user_acl(USER_ACL_REGISTERED)){
    usama_redirect('/index.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

$userID = usama_is_logged_in();

$classPublisherAd = new usamaPublisherAds();

if(isset($_POST['action']) && $_POST['action'] == 'create-publisher-ad'){
    if(!usama_check_form_token()){
        usama_redirect('/ads/create_publisher_ad.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }else{
        $_POST['adType'] = TNB_AD_TYPE_CUSTOM;
        if($classPublisherAd->savePublisherAd($userID, $_POST)){

            usama_redirect('/ads/publisher.php', $classPublisherAd->last_message);
        }else{
            usama_redirect('/ads/create_publisher_ad.php', $classPublisherAd->last_message, MSG_TYPE_ERROR);
        }
    }
}

$classAds = new usamaAds();
$adSizes = $classAds->getAdSizes();

usama_enqueue_javascript('colorpicker.js');
usama_enqueue_javascript('create_publisher_ads.js');

usama_enqueue_stylesheet('colorpicker.css');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['headerType'] = "ads";
$TNB_GLOBALS['content'] = "ads/create_publisher_ad";

$TNB_GLOBALS['title'] = "Create Publisher Ad - thenewboston Ads";

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
