<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

//Getting Current User ID
if(!usama_check_user_acl(USER_ACL_REGISTERED)){
    usama_redirect('/index.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
}

$userID = usama_is_logged_in();

$classAds = new usamaAds();

//Add Funds
if(isset($_POST['action']) && $_POST['action'] == 'add-funds'){

    if(!usama_check_form_token()){
        usama_redirect('/ads/advertiser.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }

    $adID = usama_escape_query_integer($_POST['id']);

    $adDetail = $classAds->getAdById($adID);

    if(!$adDetail || ($adDetail['ownerID'] != $userID && usama_check_user_acl(USER_ACL_MODERATOR))){
        usama_redirect('/ads/advertiser.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }

    $result = $classAds->addFunds($userID, $adID, $_POST['amount']);

    usama_add_message($classAds->last_message, $result ? MSG_TYPE_SUCCESS : MSG_TYPE_ERROR);
}

usama_enqueue_stylesheet('publisher.css');

$adID = usama_escape_query_integer($_GET['id']);

$adDetail = $classAds->getAdById($adID);

if(!$adDetail || ($adDetail['ownerID'] != $userID && usama_check_user_acl(USER_ACL_MODERATOR))){
    usama_redirect('/ads/advertiser.php');
}

$TNB_GLOBALS['headerType'] = "ads";
$TNB_GLOBALS['content'] = "ads/view";

usama_enqueue_javascript('jquery.number.js');

$TNB_GLOBALS['title'] = "View Ad - thenewboston Ads";

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
