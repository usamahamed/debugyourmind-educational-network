<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

$pageIns = new usamaPage();
$pageFollowerIns = new usamaPageFollower();
$paramPageID = isset($_GET['pid']) ? intval($_GET['pid']) : null;

$pageData = $pageIns->getPageByID($paramPageID);

//If the parameter is null, goto homepage 
if(!usama_not_null($pageData))
	usama_redirect('/index.php');

$page = isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 1;
$totalCount = $pageFollowerIns->getNumberOfFollowers($pageData['pageID']);;

$pagination = new Pagination($totalCount, usamaPageFollower::COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

//Get Friends
$view['followers'] = $pageFollowerIns->getFollowers($pageData['pageID'], $page, usamaPageFollower::COUNT_PER_PAGE);
$view['pageData'] = $pageData;

usama_enqueue_stylesheet('profile.css');
usama_enqueue_stylesheet('friends.css');

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('stream.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');
usama_enqueue_stylesheet('page.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');
usama_enqueue_javascript('jquery.color.js');

usama_enqueue_javascript('posts.js');
usama_enqueue_javascript('add_post.js');
usama_enqueue_javascript('page.js');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['content'] = 'followers';

$TNB_GLOBALS['title'] = trim($pageData['title']) . " Members - " . TNB_SITE_NAME;

//if logged user can see all resources of the current user

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
