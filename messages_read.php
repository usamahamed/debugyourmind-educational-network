<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//If the user is not logged in, redirect to the index page
if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php');
}

$userData = usamaUser::getUserBasicInfo($userID);

if(isset($_POST['action'])){
    //Check the user id is same with the current logged user id
    if($_POST['userID'] != $userID){
        echo 'Invalid Request!';
        exit;
    }
    //Delete Message
    if($_POST['action'] == 'delete_message'){
        if(!usamaMessage::deleteMessages($_POST['messageID'])){
            usama_redirect('/messages_inbox.php', "Error: " . $db->getLastError(), MSG_TYPE_ERROR);
        }else{
            usama_redirect('/messages_inbox.php', MSG_MESSAGE_REMOVED, MSG_TYPE_SUCCESS);
        }
        exit;
    }

    //Delete Message Foreer
    if($_POST['action'] == 'delete_forever'){
        if(!usamaMessage::deleteMessagesForever($_POST['messageID'])){
            usama_redirect('/messages_inbox.php', "Error: " . $db->getLastError(), MSG_TYPE_ERROR);
        }else{
            usama_redirect('/messages_inbox.php', MSG_MESSAGE_REMOVED, MSG_TYPE_SUCCESS);
        }
        exit;
    }

}

$messageID = usama_escape_query_integer(isset($_GET['message']) ? $_GET['message'] : null);

if(!$messageID){
    usama_redirect('/messages_inbox.php');
}

$message = usamaMessage::getMessage($messageID);

//If the current user is morderator and this message has been reported
if(!$message && usama_check_user_acl(USER_ACL_MODERATOR) && usamaReport::isReported($messageID, 'message')){
    //Getting Message
    $message = usamaMessage::getMessageById($messageID);

    $msgType = 'reported';
}

if(!$message){
    usama_redirect('/messages_inbox.php');
}

if(!isset($msgType)){
    //Make Message as read
    usamaMessage::changeMessageStatus($message['messageID'], 'read');

    //Getting Next Message ID and Prev Message ID
    if($message['is_trash'] == 1){
        $msgType = 'trash';
    }else if($message['receiver'] == $userID){
        $msgType = 'inbox';
    }else if($message['sender'] == $userID){
        $msgType = 'sent';
    }

    $nextID = usamaMessage::getNextID($userID, $message['messageID'], $msgType);
    $prevID = usamaMessage::getPrevID($userID, $message['messageID'], $msgType);
}
usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('info.css');
usama_enqueue_stylesheet('messages.css');

usama_enqueue_javascript('messages.js');

$TNB_GLOBALS['content'] = 'messages_read';

$TNB_GLOBALS['title'] = "Read Message - " . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
