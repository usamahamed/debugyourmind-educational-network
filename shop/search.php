<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

usama_enqueue_stylesheet('shop.css');

usama_enqueue_javascript('shop.js');

$TNB_GLOBALS['content'] = 'shop/search';
$TNB_GLOBALS['headerType'] = 'shop';

$paramCurrentPage = usama_escape_query_string($_REQUEST['page']);
$paramQueryStr = usama_escape_query_string($_REQUEST['q'], true);
$paramCategory = usama_escape_query_string($_REQUEST['cat'], true);
$paramLocation = usama_escape_query_string($_REQUEST['loc'], true);
$paramSort = usama_escape_query_string($_REQUEST['sort']);
$paramUserID = usama_escape_query_string($_REQUEST['user']);

$view = [];

//Get available products
$shopProductIns = new usamaShopProduct();
$countryIns = new usamaCountry();

$productResultList = $shopProductIns->search($paramQueryStr, $paramCategory, $paramLocation, $paramUserID);
$productResultList = $shopProductIns->sortProducts($productResultList, $paramSort);

$view['categoryList'] = $shopProductIns->countProductInCategory($productResultList);

//Create Base URL for pagination of search page
$paginationUrlBase = usama_shop_search_url($paramQueryStr, $paramCategory, $paramLocation, $paramSort, $paramUserID);

//Display

$view['products'] = fn_usama_pagination($productResultList, $paginationUrlBase, $paramCurrentPage, COMMON_ROWS_PER_PAGE);

$view['param']['q'] = $paramQueryStr;
$view['param']['cat'] = $paramCategory;
$view['param']['loc'] = $paramLocation;
$view['param']['sort'] = $paramSort;
$view['param']['user'] = $paramUserID;

$TNB_GLOBALS['shopSearchParam'] = $view['param'];

$view['countryList'] = $countryIns->getCountryList();

if($paramQueryStr != ''){
    $TNB_GLOBALS['title'] = $paramQueryStr . ' - usamaRoomShop Search';
}else if($paramCategory != ''){
    $TNB_GLOBALS['title'] = $paramCategory . ' - usamaRoomShop Search';
}else if($paramUserID != '' && is_numeric($paramUserID)){

    $userIns = new usamaUser();
    $userData = $userIns->getUserBasicInfo($paramUserID);
    if($userData){
        $TNB_GLOBALS['title'] = trim($userData['firstName'] . ' ' . $userData['lastName']) . "'s Items - usamaRoomShop Search";
    }else{
        $TNB_GLOBALS['title'] = 'usamaRoomShop Search';
    }

}else if($paramLocation != ''){
    $TNB_GLOBALS['title'] = $paramLocation . ' - usamaRoomShop Search';
}else{
    $TNB_GLOBALS['title'] = 'usamaRoomShop Search';
}

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
