<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

usama_enqueue_stylesheet('shop.css');
usama_enqueue_javascript('shop.js');

$TNB_GLOBALS['content'] = 'shop/sold';
$TNB_GLOBALS['headerType'] = 'shop';

//Update sold notification as read
$notificationIns = new usamaShopNotification();
$notificationIns->markAsRead($userID, usamaShopNotification::ACTION_TYPE_PRODUCT_SOLD);

$paramCurrentPage = get_secure_integer(isset($_REQUEST['page']) ? $_REQUEST['page'] : null);
$paramType = get_secure_string(isset($_REQUEST['type']) ? $_REQUEST['type'] : "");

$view = [];
$orderIns = new usamaShopOrder();

$view['sold'] = $orderIns->getSold($userID);

//Update Sold product as read
$orderIns->updateSoldAsRead($userID);

$view['sold'] = fn_usama_pagination($view['sold'], '/shop/sold.php', $paramCurrentPage, COMMON_ROWS_PER_PAGE);

$TNB_GLOBALS['title'] = 'My Sold Items - usamaRoomShop';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
