<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

usama_enqueue_javascript('shop.js');

usama_enqueue_stylesheet('shop.css');

$TNB_GLOBALS['content'] = 'shop/available';
$TNB_GLOBALS['headerType'] = 'shop';

$paramCurrentPage = get_secure_integer($_REQUEST['page']);
$paramType = get_secure_string($_REQUEST['type']);

$view = [];

//Get available products
$shopProductIns = new usamaShopProduct();

$baseURL = '/shop/available.php';
if($paramType == 'expired'){
    $baseURL .= "?type=" . $paramType;
}else{
    $paramType = '';
}

switch($paramType){
    case 'expired':
        $view['pagetitle'] = 'My Expired Items';
        $view['products'] = $shopProductIns->getProductList($userID, true, usamaShopProduct::STATUS_ACTIVE);
        $view['type'] = 'expired';
        break;
    case 'available':
    default:
        $view['products'] = $shopProductIns->getProductList($userID, false, usamaShopProduct::STATUS_ACTIVE);
        $view['pagetitle'] = 'My Items for Sale';
        $view['type'] = 'available';
        break;
}

$view['products'] = fn_usama_pagination($view['products'], $baseURL, $paramCurrentPage, COMMON_ROWS_PER_PAGE);

$view['type'] = $paramType;

$TNB_GLOBALS['title'] = $view['pagetitle'] . ' - usamaRoomShop';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
