<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

$bitcoinInfo = usamaUser::getUserBitcoinInfo($userID);
if(!$bitcoinInfo){
    $bitcoinInfo = usamaBitcoin::createWallet($TNB_GLOBALS['user']['userID'], $TNB_GLOBALS['user']['email']);
}

usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');
usama_enqueue_stylesheet('shop.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');
usama_enqueue_javascript('jquery.color.js');
usama_enqueue_javascript('shop.js');
usama_enqueue_javascript('shop-edit.js');
usama_enqueue_javascript('uploadify/flash_install.js');

$TNB_GLOBALS['content'] = 'shop/additem';
$TNB_GLOBALS['headerType'] = 'shop';

$view = [];

$shopCatIns = new usamaShopCategory();
$countryIns = new usamaCountry();

$view['no_cash'] = false;

$userInfo = usamaUser::getUserBasicInfo($userID);

$view['category_list'] = $shopCatIns->getCategoryList(0);
$view['country_list'] = $countryIns->getCountryList();
$view['action_name'] = 'addShopProduct';
$view['page_title'] = 'Sell an Item';
$view['type'] = 'additem';

$view['my_bitcoin_balance'] = usamaBitcoin::getUserWalletBalance($userID);
$view['my_credit_balance'] = $userInfo['credits'];

$view['shipping_fee_list'] = [];

if($view['my_bitcoin_balance'] < SHOP_PRODUCT_LISTING_FEE_IN_BTC && $view['my_credit_balance'] < SHOP_PRODUCT_LISTING_FEE_IN_CREDIT){
    $view['no_cash'] = true;
}

$TNB_GLOBALS['title'] = 'Sell an Item - usamaRoomShop';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");