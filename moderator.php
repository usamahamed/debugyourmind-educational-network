<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
if(!usama_check_user_acl(USER_ACL_REGISTERED)){
    usama_redirect('/register.php');
}

//Choose Moderator
if(isset($_GET['action'])){
    if($_GET['action'] == 'delete-candidate'){
        if(!usama_check_id_encrypted($_GET['id'], $_GET['idHash'])){
            usama_redirect('/moderator.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        usamaModerator::deleteCandidate($userID, $_GET['id']);
        usama_redirect('/moderator.php');
    }

    if($_GET['action'] == 'delete-moderator'){
        //Confirm that the user is administrator
        if(!usama_check_user_acl(USER_ACL_ADMINISTRATOR)){
            usama_redirect('/moderator.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        if(!usama_check_id_encrypted($_GET['id'], $_GET['idHash'])){
            usama_redirect('/moderator.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        usamaModerator::deleteModerator($_GET['id']);
        usama_redirect('/moderator.php');
    }
    if($_GET['action'] == 'reset-voting'){
        //Confirm that the user is administrator
        if(!usama_check_user_acl(USER_ACL_ADMINISTRATOR)){
            usama_redirect('/moderator.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        usamaModerator::resetVotes();
        usama_redirect('/moderator.php');
    }

}

//Process Actions
if(isset($_POST['action'])){
    if($_POST['action'] == 'apply_candidate'){
        if(isset($_POST['candidate_id'])){
            usamaModerator::updateCandidate($_POST['candidate_id'], $userID, $_POST['moderator_text']);
            usama_redirect('/moderator.php', MSG_UPDATE_CANDIDATE_SUCCESSFULLY);
        }else{
            $newID = usamaModerator::applyCandidate($userID, $_POST['moderator_text']);
            usama_redirect('/moderator.php', MSG_APPLY_JOB_SUCCESSFULLY);
        }

    }
    if($_POST['action'] == 'thumb-up' || $_POST['action'] == 'thumb-down'){
        if(!$_POST['candidateID'] || !$_POST['candidateIDHash'] || !usama_check_id_encrypted($_POST['candidateID'], $_POST['candidateIDHash'])){
            $data = ['status' => 'error', 'message' => MSG_INVALID_REQUEST];
        }else{
            $result = usamaModerator::voteCandidate($userID, $_POST['candidateID'], $_POST['action'] == 'thumb-up' ? true : false);
            if(is_int($result)){
                $data = ['status' => 'success', 'message' => MSG_THANKS_YOUR_VOTE, 'votes' => ($result > 0 ? "+" : "") . $result];
            }else{
                $data = ['status' => 'error', 'message' => $result];
            }
        }

        render_result_xml($data);
        exit;
    }
    if($_POST['action'] == 'add-moderator'){
        //Confirm that the user is administrator
        if(!usama_check_user_acl(USER_ACL_ADMINISTRATOR)){
            usama_redirect('/moderator.php', MSG_PERMISSION_DENIED, MSG_TYPE_ERROR);
        }

        //Check the url parameters is correct 
        if(!isset($_POST['new_moderator_id'])){
            usama_redirect('/moderator.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }

        usamaModerator::addModerator($_POST['new_moderator_id']);
        usama_redirect('/moderator.php');
    }
}

$page = isset($_GET['page']) ? $_GET['page'] : 1;
$totalCount = usamaModerator::getCandidatesCount();

//Getting Current Moderator
$currentModerators = usamaModerator::getModerators();

//Init Pagination Class
$pagination = new Pagination($totalCount, usamaModerator::$CANDIDATES_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

$candidates = usamaModerator::getCandidates($page);

//Getting My Candidate
$myCandidate = usamaModerator::getCandidate($userID);

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('moderator.css');

usama_enqueue_javascript('moderator.js');

$TNB_GLOBALS['content'] = 'moderator';

$TNB_GLOBALS['title'] = "Moderator - " . TNB_SITE_NAME;

//if logged user can see all resources of the current user

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
