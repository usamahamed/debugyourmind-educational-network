<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

//Getting Activity Stream
$stream = usamaPost::getUserPostsStream($userID);

//Get Activities
$activities = usamaActivity::getActivities($userID);
if(!$activities)
    $activities = [];

//Get Notifications
$notifications = usamaActivity::getNotifications($userID);

//Mark the notifications to read
usamaActivity::markReadNotifications($userID);

if(!$notifications)
    $notifications = [];

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('stream.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');
usama_enqueue_javascript('jquery.color.js');

usama_enqueue_javascript('posts.js');
usama_enqueue_javascript('add_post.js');
usama_enqueue_javascript('account.js');

//Set Content
$TNB_GLOBALS['content'] = 'account';

//Page Title
$TNB_GLOBALS['title'] = 'My Account - ' . TNB_SITE_NAME;
require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
