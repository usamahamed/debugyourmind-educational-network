<?php

require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

if(isset($_POST['action']) && $_POST['action'] == 'create'){
    //Check Token
    if(!usama_check_form_token()){
        usama_redirect("/page_add.php", MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }

    if(!$_POST['pageName']){
        usama_redirect("/page_add.php", MSG_PAGE_NAME_EMPTY, MSG_TYPE_ERROR);
    }

    if(!$_POST['file']){
        usama_redirect("/page_add.php", MSG_PAGE_LOGO_EMPTY, MSG_TYPE_ERROR);
    }

    if(!isset($_POST['file']) || strpos($_POST['file'], "../") !== false || !file_exists(DIR_FS_PHOTO_TMP . $_POST['file'])){
        usama_redirect("/page_add.php", MSG_FILE_UPLOAD_ERROR, MSG_TYPE_ERROR);
    }

    $fileParts = pathinfo($_POST['file']);
    if(!in_array(strtolower($fileParts['extension']), $TNB_GLOBALS['imageTypes'])){
        usama_redirect("/page_add.php", MSG_INVALID_PHOTO_TYPE, MSG_TYPE_ERROR);
        return false;
    }

    $pageClass = new usamaPage();
    if(($pageID = $pageClass->addPage($userID, $_POST))){
        usama_add_message(MSG_PAGE_CREATED_SUCCESSFULLY, MSG_TYPE_SUCCESS);
        usama_redirect("/page.php?pid=" . $pageID);
    }else{
        usama_redirect("/page_add.php");
    }

}

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('page.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');
usama_enqueue_javascript('jquery.color.js');

usama_enqueue_javascript('add_page.js');

$TNB_GLOBALS['content'] = 'page_add';

$TNB_GLOBALS['title'] = "Create a New Page - " . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");