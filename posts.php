<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

//Getting User ID from Parameter
$profileID = get_secure_integer($_GET['user']);
$postID = usama_escape_query_integer(isset($_GET['post']) ? $_GET['post'] : null);

//If the parameter is null, goto homepage
if(!$profileID)
    usama_redirect('/index.php');

//Getting UserData from Id
$userData = usamaUser::getUserData($profileID);

//Goto Homepage if the userID is not correct
if(!usama_not_null($userData) || !usamaUser::checkUserID($profileID, true)){
    usama_redirect('/index.php');
}

$postType = isset($_GET['type']) ? $_GET['type'] : 'all';
if(!in_array($postType, ['all', 'user', 'friends'])){
    $postType = 'all';
}

//if logged user can see all resources of the current user
$canViewPrivate = $userID == $profileID || usamaFriend::isFriend($userID, $profileID) || usamaFriend::isSentFriendRequest($profileID, $userID);

$posts = usamaPost::getPostsByUserID($profileID, $userID, usamaPost::INDEPENDENT_POST_PAGE_ID, $canViewPrivate, $postID, null, $postType);

/*if( !usama_not_null($posts) )
{
    //Goto Index Page
    usama_redirect('/index.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}*/

//Mark the notifications to read
if($postID)
    usamaActivity::markReadNotifications($userID, $postID);

usama_enqueue_stylesheet('profile.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');
usama_enqueue_stylesheet('publisher.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');

usama_enqueue_javascript('posts.js');

$TNB_GLOBALS['content'] = 'posts';

if($userData){
    $TNB_GLOBALS['title'] = trim($userData['firstName'] . ' ' . $userData['lastName']) . "'s Posts - " . TNB_SITE_NAME;
}

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
