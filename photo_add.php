<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

if(isset($_POST['action']) && $_POST['action'] == 'create-photo'){
    //Add Photo
    if($newID = usamaPost::savePhoto($userID, $_POST)){
        usama_redirect('/photo_edit.php?photoID=' . $newID);
    }else{
        usama_redirect('/photo_add.php');
    }

}

//Getting UserData from Id
$userData = usamaUser::getUserData($userID);

//Getting User Albums
$albums = usamaAlbum::getAlbumsByUserId($userID);

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');
usama_enqueue_javascript('jquery.color.js');
usama_enqueue_javascript('add_photo.js');

$TNB_GLOBALS['content'] = 'photo_add';

$TNB_GLOBALS['title'] = "Add Photo - " . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
