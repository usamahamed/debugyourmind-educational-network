<?php

class usamaFriendApi {

    public function getReceivedAction(){
        $request = $_GET;

        $token = isset($request['TOKEN']) ? trim($request['TOKEN']) : null;

        if(!$token){
            return ['STATUS_CODE' => STATUS_CODE_BAD_REQUEST, 'DATA' => usama_api_get_error_result('Api token should not be blank')];
        }

        if(!($userID = usamaUsersToken::checkTokenValidity($token, "api"))){
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('Api token is not valid.')];
        }

        $friends = usamaFriend::getReceivedRequests($userID);

        $results = [];

        foreach($friends as $row){
            $item = [];

            $item['id'] = $row['userID'];
            $item['name'] = $row['fullName'];
            $item['thumbnail'] = THENEWBOSTON_SITE_URL . usamaUser::getProfileIcon($row['userID']);
            $item['description'] = $row['city'];
            $item['friendType'] = $row['status'];

            $results[] = $item;
        }

        return ['STATUS_CODE' => STATUS_CODE_OK, 'DATA' => ["STATUS" => "SUCCESS", "RESULT" => $results]];
    }

    public function getPendingAction(){
        $request = $_GET;

        $token = isset($request['TOKEN']) ? trim($request['TOKEN']) : null;

        if(!$token){
            return ['STATUS_CODE' => STATUS_CODE_BAD_REQUEST, 'DATA' => usama_api_get_error_result('Api token should not be blank')];
        }

        if(!($userID = usamaUsersToken::checkTokenValidity($token, "api"))){
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('Api token is not valid.')];
        }

        $friends = usamaFriend::getPendingRequests($userID);

        $results = [];

        foreach($friends as $row){
            $item = [];

            $item['id'] = $row['userID'];
            $item['name'] = $row['fullName'];
            $item['thumbnail'] = THENEWBOSTON_SITE_URL . usamaUser::getProfileIcon($row['userID']);
            $item['description'] = $row['city'];
            $item['friendType'] = $row['status'];

            $results[] = $item;
        }

        return ['STATUS_CODE' => STATUS_CODE_OK, 'DATA' => ["STATUS" => "SUCCESS", "RESULT" => $results]];
    }

    public function acceptAction(){
        $data = $_POST;

        $token = isset($data['TOKEN']) ? trim($data['TOKEN']) : null;

        if(!$token){
            return ['STATUS_CODE' => STATUS_CODE_BAD_REQUEST, 'DATA' => usama_api_get_error_result('Api token should not be blank')];
        }

        if(!($userID = usamaUsersToken::checkTokenValidity($token, "api"))){
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('Api token is not valid.')];
        }

        if(usamaFriend::accept($userID, $data['friendId'])){
            return ['STATUS_CODE' => STATUS_CODE_OK, 'DATA' => ['STATUS' => 'SUCCESS']];
        }else{
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('There was an error to send your message.')];
        }
    }

    public function declineAction(){
        $data = $_POST;

        $token = isset($data['TOKEN']) ? trim($data['TOKEN']) : null;

        if(!$token){
            return ['STATUS_CODE' => STATUS_CODE_BAD_REQUEST, 'DATA' => usama_api_get_error_result('Api token should not be blank')];
        }

        if(!($userID = usamaUsersToken::checkTokenValidity($token, "api"))){
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('Api token is not valid.')];
        }

        if(usamaFriend::decline($userID, $data['friendId'])){
            return ['STATUS_CODE' => STATUS_CODE_OK, 'DATA' => ['STATUS' => 'SUCCESS']];
        }else{
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('There was an error to send your message.')];
        }
    }

    public function deleteAction(){
        $data = $_POST;

        $token = isset($data['TOKEN']) ? trim($data['TOKEN']) : null;

        if(!$token){
            return ['STATUS_CODE' => STATUS_CODE_BAD_REQUEST, 'DATA' => usama_api_get_error_result('Api token should not be blank')];
        }

        if(!($userID = usamaUsersToken::checkTokenValidity($token, "api"))){
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('Api token is not valid.')];
        }

        if(usamaFriend::delete($userID, $data['friendId'])){
            return ['STATUS_CODE' => STATUS_CODE_OK, 'DATA' => ['STATUS' => 'SUCCESS']];
        }else{
            return ['STATUS_CODE' => STATUS_CODE_UNAUTHORIZED, 'DATA' => usama_api_get_error_result('There was an error to send your message.')];
        }
    }
}