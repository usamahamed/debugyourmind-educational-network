<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
	usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

//Action Process
if(isset($_POST['action']) && $_POST['action'] == 'submit-post'){

	//Check Token
	if(!usama_check_form_token()){
		usama_redirect("/account.php", MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
	}

	//Save Post
	usamaPost::savePost($userID, $_POST);

	if(isset($_POST['return'])){
		usama_redirect(base64_decode($_POST['return']));
	}else{
		if(isset($_POST['pageID']) && is_numeric($_POST['pageID'])){
			usama_redirect('/page.php?pid=' . $_POST['pageID']);
		}else{
			usama_redirect('/account.php');
		}
	}

}else if(isset($_GET['action']) && $_GET['action'] == 'delete-post'){
	//Delete Post
	if($userID != $_GET['userID'] || !usamaPost::deletePost($userID, $_GET['postID'])){
		echo 'Invalid Request';
	}else{
		echo 'success';
	}
	exit;
}else if(isset($_GET['action']) && ($_GET['action'] == 'unlikePost' || $_GET['action'] == 'likePost')){
	$post = usamaPost::getPostById($_GET['postID']);
	if($post['post_status'] != 1){
		render_result_xml(['status' => 'error', 'message' => MSG_INVALID_REQUEST]);
		exit;
	}

	$r = usamaPost::likePost($userID, $_GET['postID'], $_GET['action']);
	$likes = usamaPost::getPostLikesCount($_GET['postID']);

	render_result_xml(['status' => $r ? 'success' : 'error', 'message' => usama_get_messages(), 'likes' => $likes . " like" . ($likes >= 2 ? "s" : ""), 'postID' => $_GET['postID']]);
	exit;
}