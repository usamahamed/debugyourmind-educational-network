<?php

require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

//Process Some Actions
if(isset($_GET['action']) && $_GET['action'] == 'ban-user'){
    if(!usamaModerator::isModerator($userID)){
        die(MSG_PERMISSION_DENIED);
    }

    if(!isset($_GET['userID']) || !usamaUser::checkUserID($userID)){
        usama_redirect('/index.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
    }

    //Ban User
    usamaBanUser::banUser($_GET['userID']);
    usama_redirect('/index.php', MSG_BAN_USER);
    exit;
}

//Getting User ID from Parameter
$profileID = usama_escape_query_integer(isset($_GET['user']) ? $_GET['user'] : null);

//If the parameter is null, goto homepage 
if(!$profileID)
    usama_redirect('/index.php');

//Getting UserData from Id
$userData = usamaUser::getUserData($profileID);

//Goto Homepage if the userID is not correct
if(!usama_not_null($userData) || (!usamaUser::checkUserID($profileID, true) && !usama_check_user_acl(USER_ACL_ADMINISTRATOR))){
    usama_redirect('/index.php');
}

$postType = isset($_GET['type']) ? $_GET['type'] : 'all';
if(!in_array($postType, ['all', 'user', 'friends'])){
    $postType = 'all';
}

//if logged user can see all resources of the current user
$canViewPrivate = $userID == $profileID || usamaFriend::isFriend($userID, $profileID) || usamaFriend::isSentFriendRequest($profileID, $userID);

$friends = usamaFriend::getAllFriends($profileID, 1, 18, true);
$totalFriendsCount = usamaFriend::getNumberOfFriends($profileID);

$posts = usamaPost::getPostsByUserID($profileID, $userID, usamaPost::INDEPENDENT_POST_PAGE_ID, $canViewPrivate, isset($_GET['post']) ? $_GET['post'] : null, null, $postType);

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('profile.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('publisher.css');
usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');
usama_enqueue_javascript('jquery.color.js');

usama_enqueue_javascript('posts.js');
usama_enqueue_javascript('add_post.js');
usama_enqueue_javascript('account.js');

$TNB_GLOBALS['content'] = 'profile';

//Page title
$TNB_GLOBALS['title'] = $userData['firstName'] . ' ' . $userData['lastName'] . ' - ' . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
