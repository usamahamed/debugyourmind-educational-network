<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

//Getting User ID from Parameter
$profileID = isset($_GET['user']) ? $_GET['user'] : 0;

//If the parameter is null, goto homepage 
if(!$profileID)
    usama_redirect('/index.php');

//Getting UserData from Id
$userData = usamaUser::getUserData($profileID);
$userBitcoinInfo = usamaUser::getUserBitcoinInfo($profileID);

//Goto Homepage if the userID is not correct
if(!usama_not_null($userData) || !usamaUser::checkUserID($profileID, true)){
    usama_redirect('/index.php');
}

//Display
$TNB_GLOBALS['title'] = trim($userData['firstName'] . ' ' . $userData['lastName']) . "'s Bitcoin Address - " . TNB_SITE_NAME;

usama_enqueue_stylesheet('profile.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['content'] = 'bitcoin';
$TNB_GLOBALS['meta'] = '<meta http-equiv="Pragma" content="no-cache">
                           <meta http-equiv="Cache-Control" content="no-cache">';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");

