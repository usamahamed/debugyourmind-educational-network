<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

$view = [];

//Save Shipping info
$tradeUserIns = new usamaTradeUser();

if(isset($_POST['action']) && $_POST['action'] == 'saveNotifyInfo'){
    $result = usamaUser::saveUserNotificationSettings($userID, $_POST);
    if($result === true)
        usama_redirect('/notify.php', MSG_NOTIFICATION_SETTINGS_SAVED);else
        usama_redirect('/notify.php', $result, MSG_TYPE_ERROR);
}
//Get offer_received info

$view['trade_user_info'] = $tradeUserIns->getUserByID($userID);

$userNotifyInfo = usamaUser::getUserNotificationSettings($userID);

if(empty($view['trade_user_info'])){
    usama_redirect('/index.php', MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
}

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('info.css');

$TNB_GLOBALS['content'] = 'notify';
$TNB_GLOBALS['title'] = 'Notification Settings - ' . TNB_SITE_NAME;

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
