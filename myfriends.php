<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
if(!in_array($type, ['all', 'pending', 'requested']))
    $type = 'all';

if(isset($_REQUEST['action'])){
    $return = isset($_REQUEST['return']) ? base64_decode($_REQUEST['return']) : ('/myfriends.php?type=' . $type);

    $isAjax = isset($_REQUEST['usama_ajax']) ? true : false;

    if($isAjax)
        header('Content-type: application/xml');

    $friendID = usama_escape_query_integer($_REQUEST['friendID']);

    if(!usama_check_form_token('request')){
        if($isAjax){
            $resultXML = ['status' => 'error', 'message' => MSG_INVALID_REQUEST,];
            render_result_xml($resultXML);

        }else{
            usama_redirect($return, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
        }
        exit;
    }

    if($_REQUEST['action'] == 'unfriend'){
        if(usamaFriend::unfriend($userID, $friendID)){
            if($isAjax){
                $resultXML = ['status' => 'success', 'message' => MSG_FRIEND_REMOVED, 'html' => 'Send Friend Request', 'action' => 'unfriend', 'link' => '/myfriends.php?action=request&friendID=' . $friendID . usama_get_token_param()];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_FRIEND_REMOVED);
            }
        }else{
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => $db->getLastError(),];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, $db->getLastError(), MSG_TYPE_ERROR);
            }
        }
    }else if($_REQUEST['action'] == 'decline'){
        if(usamaFriend::decline($userID, $friendID)){
            if($isAjax){
                $resultXML = ['status' => 'success', 'message' => MSG_FRIEND_REQUEST_DECLINED, 'html' => 'Send Friend Request', 'action' => 'decline-friend-request', 'link' => '/myfriends.php?action=request&friendID=' . $friendID . usama_get_token_param()];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_FRIEND_REQUEST_DECLINED);
            }
        }else{
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => $db->getLastError(),];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, $db->getLastError(), MSG_TYPE_ERROR);
            }
        }
    }else if($_REQUEST['action'] == 'accept'){
        if(usamaFriend::accept($userID, $friendID)){
            if($isAjax){
                $resultXML = ['status' => 'success', 'message' => MSG_FRIEND_REQUEST_APPROVED, 'html' => 'Unfriend', 'action' => 'accept-friend-request', 'link' => '/myfriends.php?action=unfriend&friendID=' . $friendID . usama_get_token_param()];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_FRIEND_REQUEST_APPROVED);
            }
        }else{
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => $db->getLastError(),];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, $db->getLastError(), MSG_TYPE_ERROR);
            }
        }
    }else if($_REQUEST['action'] == 'delete'){
        if(usamaFriend::delete($userID, $friendID)){
            if($isAjax){
                $resultXML = ['status' => 'success', 'message' => MSG_FRIEND_REQUEST_REMOVED, 'html' => 'Send Friend Request', 'action' => 'delete-friend-request', 'link' => '/myfriends.php?action=request&friendID=' . $friendID . usama_get_token_param()];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_FRIEND_REQUEST_REMOVED);
            }
        }else{
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => $db->getLastError(),];
                render_result_xml($resultXML);
            }else{
                usama_redirect($return, $db->getLastError(), MSG_TYPE_ERROR);
            }
        }
    }else if($_REQUEST['action'] == 'request'){
        if(!isset($friendID) || !usamaUser::checkUserID($friendID)){
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => MSG_INVALID_REQUEST,];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
            }
            exit;
        }
        if(usamaFriend::isFriend($userID, $friendID)){
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => MSG_INVALID_REQUEST,];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_INVALID_REQUEST, MSG_TYPE_ERROR);
            }
            exit;
        }
        if(usamaFriend::isSentFriendRequest($userID, $friendID)){
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => MSG_FRIEND_REQUEST_ALREADY_SENT,];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_FRIEND_REQUEST_ALREADY_SENT, MSG_TYPE_ERROR);
            }
            exit;
        }
        if(usamaFriend::isSentFriendRequest($friendID, $userID)){
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => MSG_FRIEND_REQUEST_ALREADY_RECEIVED,];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_FRIEND_REQUEST_ALREADY_RECEIVED, MSG_TYPE_ERROR);
            }
            exit;
        }

        if(!usamaUsersDailyActivity::checkUserDailyLimit($userID, "friendRequests")){
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => sprintf(MSG_DAILY_FRIEND_REQUESTS_LIMIT_EXCEED_ERROR, USER_DAILY_LIMIT_FRIEND_REQUESTS),];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, sprintf(MSG_DAILY_FRIEND_REQUESTS_LIMIT_EXCEED_ERROR, USER_DAILY_LIMIT_FRIEND_REQUESTS), MSG_TYPE_ERROR);
            }
            exit;
        }

        if(usamaFriend::sendFriendRequest($userID, $friendID)){
            if($isAjax){
                $resultXML = ['status' => 'success', 'message' => MSG_FRIEND_REQUEST_SENT, 'html' => 'Delete Friend Request', 'action' => 'send-friend-request', 'link' => '/myfriends.php?action=delete&friendID=' . $friendID . usama_get_token_param()];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, MSG_FRIEND_REQUEST_SENT);
            }
        }else{
            if($isAjax){
                $resultXML = ['status' => 'error', 'message' => $db->getLastError(),];
                render_result_xml($resultXML);

            }else{
                usama_redirect($return, $db->getLastError(), MSG_TYPE_ERROR);
            }
        }
    }
    exit;
}

//Getting UserData from Id
$userData = usamaUser::getUserData($userID);

$page = isset($_GET['page']) ? $_GET['page'] : 1;

if($type == 'all'){
    $totalCount = usamaFriend::getNumberOfFriends($userID);
}else if($type == 'pending'){
    $totalCount = usamaFriend::getNumberOfPendingRequests($userID);
}else if($type == 'requested'){
    $totalCount = usamaFriend::getNumberOfReceivedRequests($userID);
}

//Init Pagination Class
$pagination = new Pagination($totalCount, usamaFriend::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

if($type == 'all'){
    $friends = usamaFriend::getAllFriends($userID, $page, usamaFriend::$COUNT_PER_PAGE);
}else if($type == 'pending'){
    $friends = usamaFriend::getPendingRequests($userID, $page, usamaFriend::$COUNT_PER_PAGE);
}else if($type == 'requested'){
    $friends = usamaFriend::getReceivedRequests($userID, $page, usamaFriend::$COUNT_PER_PAGE);
}

usama_enqueue_stylesheet('account.css');
usama_enqueue_stylesheet('friends.css');

usama_enqueue_javascript('friends.js');

$TNB_GLOBALS['content'] = 'myfriends';

$TNB_GLOBALS['title'] = "My Friends - " . TNB_SITE_NAME;

//if logged user can see all resources of the current user

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
