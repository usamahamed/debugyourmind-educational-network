<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

usama_enqueue_stylesheet('trade.css');

usama_enqueue_javascript('trade.js');

$TNB_GLOBALS['content'] = 'trade/offer_received';
$TNB_GLOBALS['headerType'] = 'trade';

$paramCurrentPage = usama_escape_query_integer($_REQUEST['page']);
$paramTargetID = usama_escape_query_integer($_REQUEST['targetID']);

$view = [];

//Get offer_received info
$tradeOfferIns = new usamaTradeOffer();
$view['offers'] = $tradeOfferIns->getOfferReceived($userID, $paramTargetID);
$view['offers'] = fn_usama_pagination($view['offers'], '/trade/offer_received.php', $paramCurrentPage, COMMON_ROWS_PER_PAGE);

$TNB_GLOBALS['title'] = 'Offers Received - usamaRoomTrade';

//Mark the activity (offer received) as read
$tradeNotificationIns = new usamaTradeNotification();
$tradeNotificationIns->markAsRead($userID, usamaTradeNotification::ACTION_TYPE_OFFER_RECEIVED);
$tradeOfferIns->markAsRead($userID, usamaTradeOffer::STATUS_OFFER_ACTIVE);

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
