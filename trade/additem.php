<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/register.php');
}

usama_enqueue_stylesheet('uploadify.css');
usama_enqueue_stylesheet('jquery.Jcrop.css');
usama_enqueue_stylesheet('trade.css');

usama_enqueue_javascript('uploadify/jquery.uploadify.js');
usama_enqueue_javascript('jquery.Jcrop.js');
usama_enqueue_javascript('jquery.color.js');
usama_enqueue_javascript('trade.js');
usama_enqueue_javascript('trade-edit.js');
usama_enqueue_javascript('uploadify/flash_install.js');

$TNB_GLOBALS['content'] = 'trade/additem';
$TNB_GLOBALS['headerType'] = 'trade';

$view = [];

$tradeCatIns = new usamaTradeCategory();
$countryIns = new usamaCountry();
$tradeUserIns = new usamaTradeUser();

$view['no_cash'] = false;

$view['no_credits'] = false;

if(!$tradeUserIns->hasCredits($userID)){
    $view['no_credits'] = true;
}

$userInfo = usamaUser::getUserBasicInfo($userID);

$view['category_list'] = $tradeCatIns->getCategoryList(0);
$view['country_list'] = $countryIns->getCountryList();
$view['action_name'] = 'addTradeItem';
$view['page_title'] = 'Add an Item';
$view['type'] = 'additem';

$view['my_bitcoin_balance'] = usamaBitcoin::getUserWalletBalance($userID);
$view['my_credit_balance'] = $userInfo['credits'];

if($view['my_bitcoin_balance'] < TRADE_ITEM_LISTING_FEE_IN_BTC && $view['my_credit_balance'] < TRADE_ITEM_LISTING_FEE_IN_CREDIT){
    $view['no_cash'] = true;
}

$TNB_GLOBALS['title'] = 'Add an Item - usamaRoomTrade';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
