<?php
require(dirname(dirname(__FILE__)) . '/includes/bootstrap.php');

if(!($userID = usama_is_logged_in())){
    usama_redirect('/index.php', MSG_NOT_LOGGED_IN_USER, MSG_TYPE_ERROR);
}

usama_enqueue_stylesheet('trade.css');

usama_enqueue_javascript('trade.js');

$TNB_GLOBALS['content'] = 'trade/traded';
$TNB_GLOBALS['headerType'] = 'trade';

$paramCurrentPage = usama_escape_query_integer($_REQUEST['page']);
$paramType = usama_escape_query_string($_REQUEST['type']);

$view = [];

$baseURL = '/trade/traded.php';
if($paramType == 'history'){
    $baseURL .= '?type=' . $paramType;
}else{
    $paramType = 'completed';
}

//Get offer_received info
$tradeIns = new usamaTrade();
$countryIns = new usamaCountry();
$view['trades'] = $tradeIns->getTradesByUserID($userID, $paramType);
$view['trades'] = fn_usama_pagination($view['trades'], $baseURL, $paramCurrentPage, COMMON_ROWS_PER_PAGE);

$view['myID'] = $userID;

switch($paramType){
    case 'history':
        $view['pagetitle'] = 'My Trade History';

        break;
    case 'completed':
    default:
        $view['pagetitle'] = 'My Completed Trades';

        //Mark the activity (offer received) as read
        $tradeNotificationIns = new usamaTradeNotification();
        $tradeNotificationIns->markAsRead($userID, usamaTradeNotification::ACTION_TYPE_OFFER_ACCEPTED);

        break;
}

$TNB_GLOBALS['title'] = $view['pagetitle'] . ' - usamaRoomTrade';

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
