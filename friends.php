<?php
require(dirname(__FILE__) . '/includes/bootstrap.php');

//Getting Current User ID
$userID = usama_is_logged_in();

//Getting User ID from Parameter
$profileID = isset($_GET['user']) ? intval($_GET['user']) : 0;

//If the parameter is null, goto homepage 
if(!$profileID)
    usama_redirect('/index.php');

//Getting UserData from Id
$userData = usamaUser::getUserData($profileID);

//Goto Homepage if the userID is not correct
if(!usama_not_null($userData) || !usamaUser::checkUserID($profileID, true)){
    usama_redirect('/index.php');
}

$page = isset($_GET['page']) ? $_GET['page'] : 1;
$totalCount = usamaFriend::getNumberOfFriends($profileID);

$pagination = new Pagination($totalCount, usamaFriend::$COUNT_PER_PAGE, $page);
$page = $pagination->getCurrentPage();

//Get Friends
$friends = usamaFriend::getAllFriends($profileID, $page, usamaFriend::$COUNT_PER_PAGE);

usama_enqueue_stylesheet('profile.css');
usama_enqueue_stylesheet('posting.css');
usama_enqueue_stylesheet('friends.css');
usama_enqueue_stylesheet('publisher.css');

$TNB_GLOBALS['content'] = 'friends';

if($userData){
    $TNB_GLOBALS['title'] = trim($userData['firstName'] . ' ' . $userData['lastName']) . "'s Friends - " . TNB_SITE_NAME;
}

//if logged user can see all resources of the current user

require(DIR_FS_TEMPLATE . $TNB_GLOBALS['template'] . "/" . $TNB_GLOBALS['layout'] . ".php");
